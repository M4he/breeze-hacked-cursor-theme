#!/usr/bin/env python3
import math
import os

# This hacky script iterates over all src/config/*.cursor files
# to add missing sizes

# Sizes are defined as factors via the SIZE_FACTORS variable below
# they are multiplied by the base size in each file (24)

# The script will take the 'x1' line of each file as a base and
# calculate all remaining sizes according to the factors defined
# the source file will then be overwritten accordingly by keeping
# the 'x1' lines and adding all calculated ones
# (pre-existing non-'x1' lines are discarded!)

# HINT: when adding sizes here, make sure that build.sh is also
#       extended by the necessary commands and directories
#       (the '-d' part of the inkscape commands is the dpi, simply
#        multiply the factor with 90 for new size factors)

# HINT: the second and third value of each line are the click hotspot
#       x and y coordinates respectively (where in the texture the
#       mouse click will be emitted)


DIRECTORY = os.path.join('.', 'src', 'config')

SIZE_FACTORS = [1.34, 1.67, 2.0]


# rounding helper function
def _(num):
    return int(math.floor(num))


# generate a new line based on the orig_* values and a given size factor
def _form_new_line(orig_size, orig_h_x, orig_h_y, orig_path, factor):
    # calculate new sizes
    new_size = _(float(orig_size) * factor)
    new_h_x = _(float(orig_h_x) * factor) # hotspot x
    new_h_y = _(float(orig_h_y) * factor) # hotspot y
    new_factor_str = "x2"
    # adjust the directory name for the path
    # (must be in sync with build.sh)
    if factor == SIZE_FACTORS[0]:
        new_factor_str = "x1.3"
    elif factor == SIZE_FACTORS[1]:
        new_factor_str = "x1.6"
    new_relpath = orig_path.replace("x1", new_factor_str)
    # build a new line according to factor
    new_line = "%s %s %s %s\n" % (
        new_size, new_h_x, new_h_y, new_relpath  
    )
    return new_line


def build_sizes_non_animated(line):
    ret = []
    for factor in SIZE_FACTORS:
        new_line = _form_new_line(*line.split(), factor)
        ret.append(new_line)
    return ret


def build_sizes_animated(line):
    ret = {}  # dict of lines per size factor
    for factor in SIZE_FACTORS:
        # additional value for animated sets:
        # ms = millisecond delay between frames
        size, h_x, h_y, relpath, ms = line.split()
        new_line = _form_new_line(size, h_x, h_y, relpath, factor)
        new_line = new_line.replace("\n", " %s\n" % ms)  # re-add ms
        ret[factor] = new_line
    return ret


for filename in os.listdir(DIRECTORY):
    if filename.endswith(".cursor"):
        # flag whether the file will be overwritten
        # (will be set when new lines are generated)
        do_overwrite = False
        print(filename)
        fpath = os.path.join(DIRECTORY, filename)

        target_lines = []  # for non-animated cursor sets

        # because we iterate over all lines, we need to buffer the
        # sizes seperately, so that we can put them in the correct
        # order when writing back to file
        # thus, we create the following dict:
        #
        # keys = available size factors, values = list of all lines
        #
        # {
        #    1.0: [<line 1>, <line 2>, <line 3>, ...],
        #    1.3: [<line 1>, <line 2>, <line 3>, ...],
        #    ...
        #    2.0: [<line 1>, <line 2>, <line 3>, ...]
        # }
        anim_sublists = {1.0: []}  # for animated cursor sets
        with open(fpath, 'r') as f:
            for line in f:
                # take the line with x1 in it as base
                if 'x1/' in line:
                    num_spaces = line.count(' ')
                    new_lines = []

                    # non-animated cursor sets have 4 entries separated
                    # by spaces (= 3 spaces)
                    if num_spaces == 3:
                        # preserve original line as first line
                        target_lines.append(line)
                        # add all calculated new lines
                        new_lines = build_sizes_non_animated(line)
                        target_lines = target_lines + new_lines
                        do_overwrite = True

                    # animated cursor sets have 5 entries separated by
                    # spaces (= 4 spaces)
                    elif num_spaces == 4:
                        anim_sublists[1.0].append(line)
                        d = build_sizes_animated(line)
                        # integrate the new entries into the buffer dict
                        for factor in SIZE_FACTORS:
                            if not factor in anim_sublists:
                                anim_sublists[factor] = []
                            anim_sublists[factor].append(d[factor])
                        do_overwrite = True
                    else:
                        print("ERROR in file format of %s" %
                              fpath)
                        exit(1)

        # if changes happened, write them back to file
        if do_overwrite:
            with open(fpath, 'w') as f:
                if len(target_lines) > 0:  # non-animated
                    for line in target_lines:
                        f.write(line)
                else:  # else we assume animated
                    # put entries back in order
                    for factor in [1.0] + SIZE_FACTORS:
                        for entry in anim_sublists[factor]:
                            f.write(entry)
