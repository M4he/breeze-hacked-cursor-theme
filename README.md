# Breeze Hacked M4 cursor theme

This is a fork of [breeze-hacked-cursor-theme](https://github.com/codejamninja/breeze-hacked-cursor-theme) of Jam OS.

## Changes

This fork introduces the following changes to breeze-hacked-cursor-theme:

- remove transparency
- recolor to pure black + green
- fix `clean` Makefile target
- fix all cursor click coordinates
- fix some misnamed cursor links and add new links
- new in-between sizes
- introduce `gen_sizes.py` script for generating arbitrary upscale sizes
    - currently includes 30px, 40px, 50px and 60px sized cursors

## Installation

```sh
make install
```

## Dependencies

* [Inkscape](https://inkscape.org)
* [xcursorgen](https://www.x.org/archive/X11R7.7/doc/man/man1/xcursorgen.1.xhtml)

## Screenshots

![cursor theme SVG preview](src/cursors.svg)

## License

[GPL 2.0 License](LICENSE)

## Credits

* M4he - this Mod's Author
* Ken Vermette <vermette@gmail.com> - Cursor Author
* Hugo Pereira Da Costa <hugo.pereira@free.fr> - Kstyle Developer
* Andrew Lake <jamboarder@gmail.com> - Kstyle Designer
* Uri Herrera <kaisergreymon99@gmail.com> - Breeze Icon Theme
* [Jam Risser](https://jam.jamrizzi.com) <jam@jamrizzi.com> - Contributor

## Support the author

Head over [to the original author](https://github.com/codejamninja/breeze-hacked-cursor-theme#support-on-beerpay-actually-i-drink-coffee) if you want to support them.