CWD := $(shell pwd)

.PHONY: all
all: clean build

.PHONY: install
install: build
	@mkdir -p ~/.icons
	@cp -r Breeze_Hacked_M4 ~/.icons/Breeze_Hacked_M4
	@echo ::: INSTALL :::

.PHONY: build
build: Breeze_Hacked_M4
	@echo ::: BUILD :::

.PHONY: clean
clean:
	-@rm -rf build Breeze_Hacked_M4 &>/dev/null | true
	@echo ::: CLEAN :::

Breeze_Hacked_M4:
	@./build.sh
